package space.schober.ssmb.pojo;

public class Power extends MqttJson {
	public enum Detail {
		SUSPEND,
		HIBERNATE,
		SHUTDOWN,
		REBOOT,
		START
	}

	public enum Mode {
		ON,
		OFF
	}

	private Mode mode;
	private Detail details;

	public Power(Mode mode, Detail details, String topic) {
		super(topic);
		this.setMode(mode);
		this.setDetails(details);
	}

	public Power(Mode mode, Detail details) {
		this(mode, details, "power");
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public Detail getDetails() {
		return details;
	}

	public void setDetails(Detail details) {
		this.details = details;
	}
}
