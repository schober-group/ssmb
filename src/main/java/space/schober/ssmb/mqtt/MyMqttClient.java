package space.schober.ssmb.mqtt;

import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocketFactory;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import space.schober.ssmb.error.Error;
import space.schober.ssmb.fileio.FileIO;
import space.schober.ssmb.pojo.MqttJson;
import space.schober.ssmb.pojo.Status;

public class MyMqttClient {
	private String protocol;
	private String domain;
	private String fullDomain;
	private String clientId;
	private int port;
	private String user;
	private String password;
	private String rootTopic;

	private MqttClient mqttClient;
	private Logger logger = Logger.getLogger(MyMqttClient.class.getName());

	private static MyMqttClient myMqttClient;

	/**
	 * Creates a MyMqttClient object.
	 * 
	 * @param protocol          Which protocol should be used (ssl or tcp)
	 * @param domain            Domain of the MQTT-Broker
	 * @param clientId          Used MQTT-Client-ID
	 * @param port              Port of the MQTT-Broker (8883 [SSL] or 1883 [TCP])
	 * @param user              Username for MQTT
	 * @param password          Password for MQTT
	 * @param rootTopic         Set the root topic
	 * @param caCert            Path to CA-File for MQTTS
	 * @param clientCert        Path to Client-Certificate
	 * @param clientKey         Path to Client-Key
	 * @param clientKeyPassword Password for Client-Key
	 */
	private MyMqttClient(String protocol, String domain, String clientId, int port, String user, String password,
			String rootTopic, String caCert, String clientCert, String clientKey, String clientKeyPassword) {
		this.setProtocol(protocol);
		this.setDomain(domain);
		this.setFullDomain(String.format("%s://%s", this.getProtocol(), this.getDomain()));
		this.setClientId(clientId);
		this.setPort(port);
		this.setUser(user);
		this.setPassword(password);
		this.setPubTopic(rootTopic);

		try {
			mqttClient = new MqttClient(this.getFullDomain(), this.getClientId(), new MemoryPersistence());

			MqttConnectOptions connOpts = new MqttConnectOptions();

			connOpts.setUserName(this.getUser());
			connOpts.setPassword(this.getPassword().toCharArray());
			SSLSocketFactory socketFactory;

			socketFactory = SslUtil.getSocketFactory(caCert, clientCert, clientKey, clientKeyPassword);
			connOpts.setSocketFactory(socketFactory);

			mqttClient.connect(connOpts);
			this.publish(new Status(Status.State.CONNECTED));
			logger.info("Finished init!");
		} catch (FileNotFoundException e) {
			logger.log(Level.SEVERE, String.format("Could not find certificates: %s", e.toString()));
			System.exit(Error.CERTIFICATES_NOT_FOUND.getErrorcode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns an Instance of MyMqttClient
	 * 
	 * @return Instance of MyMqttClient
	 */
	public static MyMqttClient getInstance() {
		return myMqttClient;
	}

	/**
	 * Returns an Instance of MyMqttClient (with configuration properties)
	 * 
	 * @param prop Which properties should be loaded
	 * @return Instance of MyMqttClient
	 */
	public static MyMqttClient getInstance(Properties prop) {
		myMqttClient = new MyMqttClient(
				prop.getProperty(FileIO.PROP_PROTOCOL),
				prop.getProperty(FileIO.PROP_MQTT_BROKER),
				prop.getProperty(FileIO.PROP_CLIENT_ID),
				Integer.parseInt(prop.getProperty(FileIO.PROP_PORT)),
				prop.getProperty(FileIO.PROP_MQTT_USERNAME),
				prop.getProperty(FileIO.PROP_MQTT_PASSWORD),
				prop.getProperty(FileIO.PROP_DEFAULT_TOPIC),
				FileIO.getPath(prop.getProperty(FileIO.PROP_CA_CERT)),
				FileIO.getPath(prop.getProperty(FileIO.PROP_CLIENT_CERT)),
				FileIO.getPath(prop.getProperty(FileIO.PROP_CLIENT_KEY)),
				prop.getProperty(FileIO.PROP_CLIENT_KEY_PW));

		return getInstance();
	}

	/**
	 * Disconnects MQTT-Client from Broker
	 */
	public void disconnect() {
		try {
			mqttClient.disconnect();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Publishes message (on rootTopic/subTopic)
	 * 
	 * @param subTopic Which subTopic should be used.
	 * @param msg      The message to publish.
	 */
	public void publish(MqttJson obj) {
		try {
			String topic = getTopic(obj.getTopic());
			MqttMessage msg = new MqttMessage(obj.toJson());
			mqttClient.publish(topic, msg);
			logger.log(Level.INFO, String.format("[%s]: %s", topic, msg));
		} catch (MqttException | UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private String getTopic(String subtopic) throws UnknownHostException {
		return String.format("%s/%s/%s", this.getPubTopic(), InetAddress.getLocalHost().getHostName(), subtopic);
	}

	// Getter & Setter
	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getFullDomain() {
		return fullDomain;
	}

	public void setFullDomain(String fullDomain) {
		this.fullDomain = fullDomain;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPubTopic() {
		return rootTopic;
	}

	public void setPubTopic(String pubTopic) {
		this.rootTopic = pubTopic;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
}
