# SSMB

## What is SSMB?

SSMB is a little Java tool which runs on shutdown/startup and sends a MQTT message to a desired broker.


## Use cases

A home automation system (e.g. Home Assistant) can look out for those MQTT messages and if the computer is turned off, can switch off all plugs which are connected to the computer. So when you turn off your computer every peripheral/monitor is also turned off.


# How to use it

Download the SSMB.jar from the release page or compile it yourself with `mvn compile assembly:single`.
Then put it into a desired folder and edit the config files under `/etc/xdg/SSMB/config` or `C:\ProgramData\Schober\SSMB\config`. Here are some [examples](example/ssmb)
As SSMB requires MQTTS, you also need to add certificates `/etc/xdg/SSMB/certs` or `C:\ProgramData\Schober\SSMB\certs` respectively. Here is a [great guide](https://openest.io/en/2020/01/03/mqtts-how-to-use-mqtt-with-tls/) to setup MQTTS.

## Linux specific (with SystemD)

Use the installation script in the [examples](example/systemd) or write your own unit files manually. (You may have to change the installation path.)


## Windows specific

On Windows follow those [instructions](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/dn789196(v=ws.11)) to add SSMB via the Local Group Policy Editor. Unfortunately Windows Home versions are not supported :grimacing:


## Home automation system

What you do with that MQTT message is up to you, but here is an [example](example/homeassistant) for an automation if you are using Home Assistant.