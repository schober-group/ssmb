package space.schober.ssmb.pojo;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties({ "topic" })
public abstract class MqttJson {
	private String topic;

	public byte[] toJson() {
		ObjectMapper obj = new ObjectMapper();
		try {
			return obj.writeValueAsBytes(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	};

	MqttJson(String topic) {
		this.setTopic(topic);
	}

	public String getTopic() {
		return topic;
	}

	private void setTopic(String topic) {
		this.topic = topic;
	}
}
