package space.schober.ssmb.cli;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import space.schober.ssmb.pojo.Power;

public class Arguments {
	enum Args {
		SHUTDOWN("s", "shutdown", false, "Execute on shutdown"),
		REBOOT("r", "reboot", false, "Execute on reboot"),
		HIBERNATE("d", "hibernate", false, "Execute on hibernate"),
		SUSPEND("z", "suspend", false, "Execute on sleep"),
		START("o", "start", false, "Execute on start"),
		HELP("h", "help", false, "List help menu");

		Args(String opt, String longOpt, boolean hasArg, String description) {
			this.option = new Option(opt, longOpt, hasArg, description);
		}

		Option option;
	}

	private static Logger logger = Logger.getLogger(Arguments.class.getName());

	/**
	 * Parses arguments and decides what message should be sent
	 * 
	 * @param args Arguments to parse
	 * @return Which action should be taken.
	 */
	public static Power parseArguments(String[] args) {
		// create the command line parser
		CommandLineParser parser = new DefaultParser();

		// create the Options
		Options options = new Options();

		for (Args arg : Args.values()) {
			options.addOption(arg.option);
		}

		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args);

			for (Option opt : line.getOptions()) {
				if (opt.equals(Args.HELP.option)) {
					printHelp(options);
					System.exit(0);
				}

				if (opt.equals(Args.SHUTDOWN.option))
					return new Power(Power.Mode.OFF, Power.Detail.SHUTDOWN);
				if (opt.equals(Args.REBOOT.option))
					return new Power(Power.Mode.OFF, Power.Detail.REBOOT);
				if (opt.equals(Args.HIBERNATE.option))
					return new Power(Power.Mode.OFF, Power.Detail.HIBERNATE);
				if (opt.equals(Args.SUSPEND.option))
					return new Power(Power.Mode.OFF, Power.Detail.SUSPEND);
				if (opt.equals(Args.START.option))
					return new Power(Power.Mode.ON, Power.Detail.START);
			}
		} catch (ParseException exp) {
			logger.warning(String.format("Could not parse arguments: %s", exp.toString()));
		}

		logger.log(Level.INFO, "No valid argument found! Using defaults");
		return new Power(Power.Mode.OFF, Power.Detail.SHUTDOWN);
	}

	/**
	 * Prints help
	 * 
	 * @param options Which options are available.
	 */
	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.setLongOptPrefix(" --");

		StringWriter out = new StringWriter();
		PrintWriter pw = new PrintWriter(out);

		formatter.printHelp(pw, 80, "SmarteSteckdoseMQTTBridge", "Sends a msg via MQTT to your desired broker...",
				options, formatter.getLeftPadding(), formatter.getDescPadding(), "", true);
		pw.flush();
		System.out.println(out.toString());
	}
}
