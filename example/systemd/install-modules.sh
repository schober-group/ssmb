#!/bin/sh

if [[ $EUID > 0 ]]
  then echo "Please run as root"
  exit
fi

services=(*.service)

for service in ${services[@]}; do
    echo "Installing $service..."
    cp $service /etc/systemd/system/
done

systemctl daemon-reload

for service in ${services[@]}; do
    echo "Enabling $service..."
    systemctl disable $service
    systemctl enable $service
done