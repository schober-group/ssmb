package space.schober.ssmb.main;

import space.schober.ssmb.cli.Arguments;
import space.schober.ssmb.fileio.FileIO;
import space.schober.ssmb.mqtt.MyMqttClient;
import space.schober.ssmb.pojo.Power;

public class Main {

	public static void main(String[] args) {
		Power p = Arguments.parseArguments(args);

		MyMqttClient mmc = MyMqttClient.getInstance(FileIO.readProperties());
		mmc.publish(p);
		mmc.disconnect();
	}
}
