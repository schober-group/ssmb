package space.schober.ssmb.pojo;

public class Status extends MqttJson {

	public enum State {
		CONNECTED,
		DISCONNECTED
	}

	private State state;

	public Status(State state, String topic) {
		super(topic);
		this.setState(state);
	}

	public Status(State state) {
		this(state, "status");
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
