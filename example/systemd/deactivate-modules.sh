#!/bin/sh

if [[ $EUID > 0 ]]
  then echo "Please run as root"
  exit
fi

services=(*.service)
systemctl daemon-reload

for service in ${services[@]}; do
    echo "Disabling $service..."
    systemctl disable $service
done