package space.schober.ssmb.error;

public enum Error {
	CONFIG_FILES_NOT_FOUND(-1),CERTIFICATES_NOT_FOUND(-2);
	
	private Error(int errorcode) {
		this.setErrorcode(errorcode);
	}
	
	private int errorcode;

	public int getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
}
