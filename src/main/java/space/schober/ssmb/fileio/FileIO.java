package space.schober.ssmb.fileio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Logger;

import net.harawata.appdirs.AppDirs;
import net.harawata.appdirs.AppDirsFactory;
import space.schober.ssmb.error.Error;

public class FileIO {
	public static final String PROP_PROTOCOL = "protocol";
	public static final String PROP_MQTT_BROKER = "mqttBroker";
	public static final String PROP_CLIENT_ID = "clientId";
	public static final String PROP_PORT = "port";
	public static final String PROP_MQTT_USERNAME = "mqttUsername";
	public static final String PROP_MQTT_PASSWORD = "mqttPassword";
	public static final String PROP_DEFAULT_TOPIC = "defaultTopic";

	public static final String PROP_CA_CERT = "caCert";
	public static final String PROP_CLIENT_CERT = "clientCert";
	public static final String PROP_CLIENT_KEY = "clientKey";
	public static final String PROP_CLIENT_KEY_PW = "clientKeyPassword";

	private static final String[] PROPERTIES_CONFIG = { PROP_PROTOCOL, PROP_MQTT_BROKER, PROP_CLIENT_ID, PROP_PORT,
			PROP_MQTT_USERNAME, PROP_DEFAULT_TOPIC, PROP_CA_CERT, PROP_CLIENT_CERT, PROP_CLIENT_KEY };

	private static final String[] PROPERTIES_SECRET = { PROP_MQTT_PASSWORD, PROP_CLIENT_KEY_PW };

	private static final String CONFIG_FILE = "config.properties";
	private static final String SECRETS_FILE = "secrets.properties";

	private static Logger logger = Logger.getLogger(FileIO.class.getName());

	/**
	 * Reads all properties
	 * 
	 * @return Properties
	 */
	public static Properties readProperties() {
		Properties prop = new Properties();
		String path = getPath("config");

		loadProperties(path, CONFIG_FILE, prop, PROPERTIES_CONFIG);
		loadProperties(path, SECRETS_FILE, prop, PROPERTIES_SECRET);

		return prop;
	}

	/**
	 * Reads properties from file and creates a template if not available.
	 * 
	 * @param path        Where the property file is located.
	 * @param filename    Name of the property file.
	 * @param prop        Object to which other properties will be appended.
	 * @param defaultProp Which properties are expected.
	 */
	private static void loadProperties(String path, String filename, Properties prop, String[] defaultProp) {
		String file = getPath(path, filename);

		try (InputStream input = new FileInputStream(file)) {
			prop.load(input);
		} catch (FileNotFoundException ex) {
			logger.severe(String.format("Config files [%s] not found...", file));
			createConfigFile(path, file, defaultProp);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Creates a template for the property file.
	 * 
	 * @param path        Where the file should be located.
	 * @param file        Name for the property file.
	 * @param defaultProp Which values the file should include.
	 */
	private static void createConfigFile(String path, String file, String[] defaultProp) {
		try {
			Files.createDirectories(Paths.get(path));
			FileWriter myWriter = new FileWriter(file);

			for (String property : defaultProp) {
				myWriter.write(String.format("%s=changeme%n", property));
			}

			myWriter.close();
			System.exit(Error.CONFIG_FILES_NOT_FOUND.getErrorcode());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets path of configuration files. Dynamically uses C:\ProgramData or /etc/xdg
	 * depending on the OS
	 * 
	 * @param subfolder Which subfolder should be used.
	 * @return The path to the configuration files.
	 */
	public static String getPath(String subfolder) {
		final AppDirs appDirs = AppDirsFactory.getInstance();
		return getPath(appDirs.getSiteConfigDir("SSMB", null, "Schober"), subfolder);
	}

	/**
	 * Returns a path to a subfolder.
	 * 
	 * @param mainPath Path in which the folder is located.
	 * @param subPath  Folder to append to path.
	 * @return Newly generated path.
	 */
	private static String getPath(String mainPath, String subPath) {
		return String.format("%s%s%s", mainPath, java.io.File.separator, subPath);
	}

	private FileIO() {
	}
}
